import os

from flask import Flask, render_template, request, redirect

from inference import get_best_match

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            return redirect(request.url)
        file = request.files['file']
        if not file:
            return
        img_bytes = file.read()
        _class, _name = get_best_match(image_bytes=img_bytes)
        return render_template("result.html", name=_name.title(),
                               class_name=_class)
    return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=True, port=int(os.environ.get('PORT', 5000)))
