import json

from commons import get_model, get_tensor

model = get_model()


with open('cat_to_name.json') as f:
    cat_to_name = json.load(f)


with open('class_to_idx.json') as f:
    class_to_idx = json.load(f)


idx_to_class = {v: k for k, v in class_to_idx.items()}


def get_best_match(image_bytes):
    try:
        tensor = get_tensor(image_bytes=image_bytes)
        outputs = model.forward(tensor)
    except Exception:
        return 0, "error"
    _, y_hat = outputs.max(1)
    predicted_idx = y_hat.item()
    predicted_class = idx_to_class[predicted_idx]
    predicted_name = cat_to_name[predicted_class]
    return predicted_class, predicted_name
