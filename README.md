# Flask Flower

## How it works? 

We send the image via form upload and on the backend it is converted to a tensor using pillow library and transformations are applied. Then it is passed through a simple model built on top of Densenet121:

```python
model = models.densenet121(pretrained=True)
model.classifier = nn.Linear(1024, 102)
```

and the forward pass:

```python
outputs = model.forward(tensor)
prediction = outputs.max(1)
```

## Requirements

Install them from `requirements.txt`:

    pip install -r requirements.txt


## Deployment

Run the server:

    python app.py
